/* code for the "rbmMIDI" pd class. */

#include "m_pd.h"
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#define NSTEPS 16
#define NBITS 7
#define NVISIBLE 336
#define NHIDDEN 700
//#define NVISIBLE 6
//#define NHIDDEN 3

#define OFFSETK 1
#define OFFSETS 113
#define OFFSETH 225
 
typedef struct rbmMIDI
{
    t_object x_ob;
    t_outlet *x_outlet0;  // kicks
    t_outlet *x_outlet1;  // snares
    t_outlet *x_outlet2;  // hihats
    
    int * kicksMIDI;
    int * snaresMIDI;
    int * hhMIDI;
    
    int * kicksBits;
    int * snaresBits;
    int * hhBits;
    
    int * dataBits;
    int * hiddenStates;
    int * visibleStates;
    
    float ** weights;
    
    
    t_atom * generatedKicks;
    t_atom * generatedSnares;
    t_atom * generatedHh;
    
    /*
    // rbmMIDI parameters
    int Npopulation;
    int strLen;
    float crossoverProb;
    float mutationProb;
    float targetFitness;
    float inputBlend;
    
    // rbmMIDI structures
    int *input;
    int **pop;
    int **newPop;
    int **parents;
    int **children;
    float *distance;
    float *selectionCDF;
    float *selectionProb;
    int shouldTerminate[3];
     */
    
    
} t_rbmMIDI;

t_class *rbmMIDI_class;

/* function prototypes */
// pd functions
void rbmMIDI_bang(t_rbmMIDI *x);
void rbmMIDI_genreMode(t_rbmMIDI *x, t_symbol *selector, int argc, t_atom *argv);
void rbmMIDI_list_kicks(t_rbmMIDI *x, t_symbol *selector, int argc, t_atom *argv);
void rbmMIDI_list_snares(t_rbmMIDI *x, t_symbol *selector, int argc, t_atom *argv);
void rbmMIDI_list_hihats(t_rbmMIDI *x, t_symbol *selector, int argc, t_atom *argv);
void *rbmMIDI_new(void);
// RBM helper functinos
void dec2Bit(int d, int * bits, int n_bits);
int bit2Dec(int * bits, int n_bits);
void run_visible(int * data, int * hiddenStates, float **weights);
void run_hidden(int * hiddenStates, int * visibleStates, float **weights);


// left inlet bang callback
void rbmMIDI_bang(t_rbmMIDI *x)
{
    
    // the three voices are concatenated in x->dataBits
    // run visible and hidden to get generated visible states
    run_visible(x->dataBits, x->hiddenStates, x->weights);
    run_hidden(x->hiddenStates, x->visibleStates, x->weights);
    
    post("x->visibleStates: %d %d %d %d %d %d %d", x->visibleStates[1], x->visibleStates[2], x->visibleStates[3],
                                                   x->visibleStates[4], x->visibleStates[5], x->visibleStates[6],
         x->visibleStates[7]);
    
    
    // separate into voices and go from binary sequence to MIDI sequence
    int i, j, k, s, h;
    int * bits = (int *)malloc(NBITS * sizeof(int));
    for(i = 0; i < NSTEPS; i++)
    {
        for(j=0; j < NBITS; j++)
            bits[j] = x->visibleStates[OFFSETK + (i*NBITS) + j];
        k = bit2Dec(bits, NBITS);
        
        for(j=0; j < NBITS; j++)
            bits[j] = x->visibleStates[OFFSETS + (i*NBITS) + j];
        s = bit2Dec(bits, NBITS);
        
        for(j=0; j < NBITS; j++)
            bits[j] = x->visibleStates[OFFSETH + (i*NBITS) + j];
        
        h = bit2Dec(bits, NBITS);
        
        SETFLOAT(x->generatedKicks+i, k);
        SETFLOAT(x->generatedSnares+i, s);
        SETFLOAT(x->generatedHh+i, h);
    }
   
    free(bits);
    
    
    // output
    
    outlet_list(x->x_outlet0, gensym("list"), NSTEPS, x->generatedKicks);
    outlet_list(x->x_outlet1, gensym("list"), NSTEPS, x->generatedSnares);
    outlet_list(x->x_outlet2, gensym("list"), NSTEPS, x->generatedHh);
     
    post("bang");
    
    
    /* DEBUG FOR RUN_VISIBLE() AND RUN_HIDDEN() */
    // test run_hidden and run_visible on a smaller set
    // make sure to set NHIDDEN to 3 and NVISIBLE to 6
    // set the weights file to smallRBMWeights.txt
    /*
    int * data = (int *)malloc(sizeof(int)*(NVISIBLE+1));
    data[0] = 1; data[1] = 1; data[2] = 0; data[3] = 0; data[4] = 1; data[5] = 0; data[6] = 1;
    
    run_visible(data, x->hiddenStates, x->weights);
    post("x->hiddenStates: %d %d %d",
         x->hiddenStates[1], x->hiddenStates[2], x->hiddenStates[3]);
    
    run_hidden(x->hiddenStates, x->visibleStates, x->weights);
    post("x->visibleStates: %d %d %d %d %d %d %d", x->visibleStates[0],
         x->visibleStates[1], x->visibleStates[2],
         x->visibleStates[3], x->visibleStates[4],
         x->visibleStates[5], x->visibleStates[6]);
     
    free(data);

    */
}

// list callbacks

void rbmMIDI_genreMode(t_rbmMIDI *x, t_symbol *selector, int argcount, t_atom *argvec)
{
    
    int i, v, h, genreMode;
    if(argcount == 1)
    {
        if(argvec[0].a_type == A_FLOAT)
        {
            genreMode = argvec[0].a_w.w_float;
            
            if(genreMode == 0)
            {
                // read in RBM weights from text file
                FILE *myFile;
		myFile = fopen("Users/jenniferhsu/Documents/repos/rbmDrumSequencer/weights.txt", "r");
                
                //read file into array
                if (myFile == NULL)
                {
                    post("Error reading file\n");
                    exit(0);
                }
                
                float d;
                for (i = 0; i < (NVISIBLE+1)*(NHIDDEN+1); i++)
                {
                    v = (int)i / (NHIDDEN+1);
                    h = (int)i % (NHIDDEN+1);
                    fscanf(myFile, "%f\n", &d);
                    
                    x->weights[v][h] = d;
                }
                
                post("weights have been changed to weights.txt!");
                
            } else if(genreMode == 1)
            {
                // read in RBM weights from text file
                FILE *myFile;
                myFile = fopen("Users/jenniferhsu/Documents/repos/rbmDrumSequencer/weights2.txt", "r");
                
                
                //read file into array
                if (myFile == NULL)
                {
                    post("Error reading file\n");
                    exit(0);
                }
                
                float d;
                for (i = 0; i < (NVISIBLE+1)*(NHIDDEN+1); i++)
                {
                    v = (int)i / (NHIDDEN+1);
                    h = (int)i % (NHIDDEN+1);
                    fscanf(myFile, "%f\n", &d);
                    
                    x->weights[v][h] = d;
                }
                
                post("weights have been changed to weights2.txt!");
                
            }
            
        }
    }
}

void rbmMIDI_list_kicks(t_rbmMIDI *x, t_symbol *selector, int argc, t_atom *argv)
{
    
    int i, j, d;

    // exit if we don't have the correct number of steps plus print to console
    if(argc != NSTEPS)
    {
        post("input should be a list of length %d", NSTEPS);
        return;
    }
    
    // parse input list and save to our array
    for(i = 0; i < argc; i++)
    {
        if(i > NSTEPS)
            return;
            
        // error check
        if(argv[i].a_type != A_FLOAT)
        {
            post("arguments in list must be numbers");
            return;
        }
        x->kicksMIDI[i] = atom_getfloatarg(i, argc, argv);
    }
    
    // convert the MIDI representation to bits
    int * bits = (int *)malloc(NBITS * sizeof(int));
    for(i = 0; i < NSTEPS; i++)
    {
        d = x->kicksMIDI[i];
        dec2Bit(d, bits, NBITS);
        // copy into our bits array
        for(j = 0; j < NBITS; j++)
        {
            x->kicksBits[(i*NBITS)+j] = bits[j];
            x->dataBits[OFFSETK + ((i*NBITS)+j)] = bits[j];
        }
    }
    
    // debug: convert the bits representation back into decimals
    /*
    int testList[NSTEPS];
    for(i = 0; i < NSTEPS; i++)
    {
        for(j = 0; j < NBITS; j++)
        {
            bits[j] = x->kicksBits[(i*NBITS)+j];
        }
        d = bit2Dec(bits, NBITS);
        // copy into our bits array
        testList[i] = d;
    }
    post("kicks list converted back to decimal: %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d",
         testList[0], testList[1], testList[2], testList[3], testList[4], testList[5], testList[6],
         testList[7], testList[8], testList[9], testList[10], testList[11], testList[12], testList[13],
         testList[14], testList[15]);
     */

    free(bits);
    
    /*
    // output the fittest one right now (newPop[shouldTerminate[1]])
    t_atom fittestInd[x->strLen];
    for(i = 0; i < x->strLen; i++)
    {
        SETFLOAT(fittestInd+i, x->newPop[x->shouldTerminate[1]][i]);
    }
    outlet_list(x->x_outlet, gensym("list"), x->strLen, fittestInd);
    */
    
    post("kicks list callback");
}


void rbmMIDI_list_snares(t_rbmMIDI *x, t_symbol *selector, int argc, t_atom *argv)
{
    
    int i, j, d;
    
    // exit if we don't have the correct number of steps plus print to console
    if(argc != NSTEPS)
    {
        post("input should be a list of length %d", NSTEPS);
        return;
    }
    
    // parse input list and save to our array
    for(i = 0; i < argc; i++)
    {
        if(i > NSTEPS)
            return;
        
        // error check
        if(argv[i].a_type != A_FLOAT)
        {
            post("arguments in list must be numbers");
            return;
        }
        x->snaresMIDI[i] = atom_getfloatarg(i, argc, argv);
    }
    
    // convert the MIDI representation to bits
    int * bits = (int *)malloc(NBITS * sizeof(int));
    for(i = 0; i < NSTEPS; i++)
    {
        d = x->snaresMIDI[i];
        dec2Bit(d, bits, NBITS);
        // copy into our bits array
        for(j = 0; j < NBITS; j++)
        {
            x->snaresBits[(i*NBITS)+j] = bits[j];
            x->dataBits[OFFSETS + ((i*NBITS)+j)] = bits[j];
        }
    }
    
    free(bits);
    
    post("snares list callback");
}


void rbmMIDI_list_hihats(t_rbmMIDI *x, t_symbol *selector, int argc, t_atom *argv)
{
    
    int i, j, d;
    
    // exit if we don't have the correct number of steps plus print to console
    if(argc != NSTEPS)
    {
        post("input should be a list of length %d", NSTEPS);
        return;
    }
    
    // parse input list and save to our array
    for(i = 0; i < argc; i++)
    {
        if(i > NSTEPS)
            return;
        
        // error check
        if(argv[i].a_type != A_FLOAT)
        {
            post("arguments in list must be numbers");
            return;
        }
        x->hhMIDI[i] = atom_getfloatarg(i, argc, argv);
    }
    
    // convert the MIDI representation to bits
    int * bits = (int *)malloc(NBITS * sizeof(int));
    for(i = 0; i < NSTEPS; i++)
    {
        d = x->hhMIDI[i];
        dec2Bit(d, bits, NBITS);
        // copy into our bits array
        for(j = 0; j < NBITS; j++)
        {
            x->hhBits[(i*NBITS)+j] = bits[j];
            x->dataBits[OFFSETH + ((i*NBITS)+j)] = bits[j];
        }
    }
    
    free(bits);
    
    post("hihats list callback");
}


// new/constructor function
void *rbmMIDI_new(void)
{
    t_rbmMIDI *x = (t_rbmMIDI *) pd_new(rbmMIDI_class);
    post("new rbmMIDI object");
    
    // create inlets
    inlet_new(&x->x_ob, &x->x_ob.ob_pd, gensym("list"), gensym("kicks"));
    inlet_new(&x->x_ob, &x->x_ob.ob_pd, gensym("list"), gensym("snares"));
    inlet_new(&x->x_ob, &x->x_ob.ob_pd, gensym("list"), gensym("hihats"));
    
    // create outlets, save pointers returned
    x->x_outlet0 = outlet_new(&x->x_ob, gensym("list"));    // kicks
    x->x_outlet1 = outlet_new(&x->x_ob, gensym("list"));    // snares
    x->x_outlet2 = outlet_new(&x->x_ob, gensym("list"));    // hihats
    
    /* initialize variables */
    int i, v, h;
    
    // seed the random number generator
    srand(time(NULL));
    //srand(123);
    
    // allocate space for arrays
    x->kicksMIDI = (int *)malloc(NSTEPS * sizeof(int));
    x->snaresMIDI = (int *)malloc(NSTEPS * sizeof(int));
    x->hhMIDI = (int *)malloc(NSTEPS * sizeof(int));
    
    x->kicksBits = (int *)malloc(NSTEPS * NBITS * sizeof(int));
    x->snaresBits  = (int *)malloc(NSTEPS * NBITS * sizeof(int));
    x->hhBits = (int *)malloc(NSTEPS * NBITS * sizeof(int));
    
    x->dataBits = (int *)malloc(((NSTEPS * NBITS * 3) + 1) * sizeof(int));
    x->hiddenStates = (int *)malloc((NHIDDEN+1) * sizeof(int));
    x->visibleStates = (int *)malloc((NVISIBLE+1) * sizeof(int));
    
    x->weights = (float **)malloc((NVISIBLE+1) * sizeof(float *));
    for(i = 0; i < NVISIBLE+1; i++)
        x->weights[i] = (float *)malloc((NHIDDEN+1) * sizeof(float));
    
    x->generatedKicks = (t_atom *)malloc(NSTEPS * sizeof(t_atom));
    x->generatedSnares = (t_atom *)malloc(NSTEPS * sizeof(t_atom));;
    x->generatedHh = (t_atom *)malloc(NSTEPS * sizeof(t_atom));;
    
    
    // zero out arrays
    for(i = 0; i < NSTEPS; i++)
    {
        x->kicksMIDI[i] = 0;
        x->snaresMIDI[i] = 0;
        x->hhMIDI[i] = 0;
    }
    
    for(i = 0; i < NSTEPS * NBITS; i++)
    {
        x->kicksBits[i] = 0;
        x->snaresBits[i] = 0;
        x->hhBits[i] = 0;
        
        x->dataBits[i] = 0;
        x->dataBits[i*2] = 0;
        x->dataBits[i*3] = 0;
    }
    
    for(i = 0; i < NHIDDEN+1; i++)
        x->hiddenStates[i] = 0;
    
    for(i = 0; i < NVISIBLE+1; i++)
        x->visibleStates[i] = 0;
    
    for(v=0; v < NVISIBLE+1; v++)
    {
        for(h=0; h < NHIDDEN+1; h++)
            x->weights[v][h] = 0.0f;
    }
    
    
    // set bias bits to 1
    x->dataBits[0] = 1;
    x->hiddenStates[0] = 1;
    x->visibleStates[0] = 1;
    
    
    // read in RBM weights from text file
    FILE *myFile;
    myFile = fopen("Users/jenniferhsu/Documents/repos/rbmDrumSequencer/weights.txt", "r");

    
    //read file into array
    if (myFile == NULL)
    {
        post("Error reading file\n");
        exit(0);
    }
 
    float d;
    for (i = 0; i < (NVISIBLE+1)*(NHIDDEN+1); i++)
    {
        v = (int)i / (NHIDDEN+1);
        h = (int)i % (NHIDDEN+1);
        fscanf(myFile, "%f\n", &d);
        
        x->weights[v][h] = d;
        //fscanf(myFile, "%f\n", &(x->weights[v][h]));
    }
    
    
    // test dec2Bit() and bit2Dec() functions
    /*
    int * bits = (int *)malloc(NBITS * sizeof(int));
    int d = 13;
    dec2Bit(d, bits, NBITS);
    int i;
    post("%d %d %d %d %d %d %d", bits[0], bits[1], bits[2], bits[3], bits[4], bits[5], bits[6]);
    int d2 = bit2Dec(bits, NBITS);
    post("d: %d, d2: %d", d, d2);
    free(bits);
     */
    
    return (void *)x;
    
}

// setup function
void rbmMIDI_setup(void)
{
    // create a new class
    rbmMIDI_class = class_new(gensym("rbmMIDI"), (t_newmethod)rbmMIDI_new,
    	0, sizeof(t_rbmMIDI), 0, A_GIMME, 0);
    
    // register the bang callback
    class_addbang(rbmMIDI_class, (t_method)rbmMIDI_bang);
    class_addmethod(rbmMIDI_class, (t_method)rbmMIDI_genreMode, gensym("genreMode"), A_GIMME, 0);
    class_addmethod(rbmMIDI_class, (t_method)rbmMIDI_list_kicks, gensym("kicks"), A_GIMME, 0);
    class_addmethod(rbmMIDI_class, (t_method)rbmMIDI_list_snares, gensym("snares"), A_GIMME, 0);
    class_addmethod(rbmMIDI_class, (t_method)rbmMIDI_list_hihats, gensym("hihats"), A_GIMME, 0);
    //class_addlist(rbmMIDI_class, (t_method)rbmMIDI_list_kicks);
    //class_addlist(rbmMIDI_class, (t_method)rbmMIDI_list_snares);
    //class_addlist(rbmMIDI_class, (t_method)rbmMIDI_list_hihats);
    
    // callback for the "set" message
    //class_addmethod(rbmMIDI_class, (t_method)rbmMIDI_set, gensym("set"), A_GIMME, 0);
    // register the list callback
    //class_addlist(rbmMIDI_class, (t_method)rbmMIDI_list);
    
    
}

/* === RBM generation helper functions === */

/* dec2Bit
 d: the decimal number to convert
 bits: a preallocated array with enough space for n_bits as ints
 n_bits: the number of bits in which we want to express the decimal number
*/
void dec2Bit(int d, int * bits, int n_bits)
{
    int remainder;
    int i = 0;
    
    // this is the divide by 2 algorithm
    while(d != 0)
    {
        remainder = d%2;
        d = d/2;
        bits[n_bits-i-1] = remainder;
        i++;
    }
    
    while(i < n_bits)
    {
        bits[n_bits-i-1] = 0;
        i++;
    }
        
}

/* bit2Dec
 bits: an array with the bit representation that we want to convert (length = n_bits)
 n_bits: the number of bits in the bits array
 */
int bit2Dec(int * bits, int n_bits)
{
    int i;
    int dec = 0;
    for(i = 0; i < n_bits; i++)
    {
        dec = dec + (bits[i] * pow(2, (n_bits-i-1)));
    }
    
    return dec;
}


/* runVisible
 */
void run_visible(int * data, int * hiddenStates, float **weights)
{
    
    int h, v;
    float hiddenActivations[NHIDDEN+1];
    float hiddenProbs[NHIDDEN+1];
    
    // data already has bias unit
    
    // initialize hidden states and bias to 1
    for(h = 0; h < NHIDDEN+1; h++)
        hiddenStates[h] = 1;
    
    // calculate activation of hidden units
    float rowSum;
    post("hiddenActivations:");
    for(h = 0; h < NHIDDEN+1; h++)
    {
        rowSum = 0;
        for(v = 0; v < NVISIBLE+1; v++)
        {
            rowSum += (data[v] * weights[v][h]);
        }
        hiddenActivations[h] = rowSum;
    }
    
    // calculate probabilites of turning hidden units on (with logistic function)
    post("hiddenProbs:");
    for(h = 0; h < NHIDDEN+1; h++)
    {
        hiddenProbs[h] = 1.0f / (1.0f + exp(-hiddenActivations[h]));
    }
    
    // turn hidden units on according to the hidden node probabilities and flipping a coin
    for(h = 0; h < NHIDDEN+1; h++)
    {
        if(hiddenProbs[h] > (float)rand()/(float)RAND_MAX)
            hiddenStates[h] = 1;
        else
            hiddenStates[h] = 0;
    }
    
    // use the hidden states with the bias remove (the first column should be omitted)
    
}


void run_hidden(int * hiddenStates, int * visibleStates, float **weights)
{
    int h, v;
    float visibleActivations[NVISIBLE+1];
    float visibleProbs[NVISIBLE+1];
    
    // hiddenStates already has the bias unit
    
    // initialize visible states and bias to 1
    for(v = 0; v < NVISIBLE+1; v++)
        visibleStates[v] = 1;
    
    // calculate activation of visible units
    float rowSum;
    for(v = 0; v < NVISIBLE+1; v++)
    {
        rowSum = 0;
        for(h = 0; h < NHIDDEN+1; h++)
        {
            rowSum += (hiddenStates[h] * weights[v][h]);
        }
        visibleActivations[v] = rowSum;
    }
    
    // calculate probabilites of turning visible units on (with logistic function)
    for(v = 0; v < NVISIBLE+1; v++)
        visibleProbs[v] = 1.0f / (1.0f + exp(-visibleActivations[v]));

    // turn visible units on according to the visible node probabilities and flipping a coin
    for(v = 0; v < NVISIBLE+1; v++)
    {
        if(visibleProbs[v] > (float)rand()/(float)RAND_MAX)
            visibleStates[v] = 1;
        else
            visibleStates[v] = 0;
    }
    
    // use visible states without the bias
    
}





